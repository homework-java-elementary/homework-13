package org.example.honework13;

import java.io.*;
import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        writeInFile("Start program", false);

        HttpClient httpClient = HttpClient.newBuilder().build();
        HeaderReader headerReader = new HeaderReader(httpClient);

        ExecutorService executorService = Executors.newFixedThreadPool(8);

        List<String> strings = readOutFile();
        for (String string : strings) {
            executorService.submit(() -> {
                writeInFile(string + "\n" + headerReader.getDecorateHeaders(string), true);
            });
        }

        executorService.shutdown();

        System.out.println("Work has done");
    }

    public static List<String> readOutFile() {
        try (BufferedReader r = new BufferedReader(new FileReader("a.txt"))) {
            List<String> strings = new ArrayList<>();
            for (String result = r.readLine(); result != null; result = r.readLine()) {
                strings.add(result);
            }
            return strings;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public synchronized static void writeInFile(String string, boolean append) {
        try (PrintWriter w = new PrintWriter(new FileWriter("out.txt", append))) {
            w.print(string + "\n");
            w.flush();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
